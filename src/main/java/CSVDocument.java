import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class CSVDocument {
	private List<List<String>> lines;
	private final CSVDocumentType documentType;
	public CSVDocument(CSVDocumentType documentType) {
		this.documentType = documentType;
		lines = new ArrayList<>();
	}

	public void readLinesFromFile(Path file, Charset charset) throws IOException, CSVParserException {
		List<String> linesFromFile = Files.readAllLines(file, charset);
		if(linesFromFile.isEmpty()) {
			throw new IOException("empty input file");
		}
		List<String> headLineFromFile = new ArrayList<>(List.of(linesFromFile.get(0).split(documentType.getSeparator())));
		headLineFromFile.removeIf(String::isEmpty);
		if(!documentType.getHeadline().equals(headLineFromFile)){
			throw new CSVParserException("headline missmatch");
		}
		for(int i = 1; i < linesFromFile.size(); i++){
			List<String> line = new ArrayList<>(List.of(linesFromFile.get(i).split(documentType.getSeparator())));
			lines.add(line);
		}
	}

	public void writeLinesToFile(Path file) throws IOException {
		StringBuilder content = new StringBuilder(String.join(documentType.getSeparator(), documentType.getHeadline()) + "\n");
		for(List<String> line: lines) {
			content.append(String.join(documentType.getSeparator(), line)).append("\n");
		}
		Files.writeString(file, content.toString(), StandardCharsets.UTF_16);
	}

	private void addLine(List<String> line) {
		lines.add(line);
	}

	public void filterLines(Predicate<List<String>> filterFunction) {
		lines = new ArrayList<>(lines.stream().filter(filterFunction).toList());
	}

	public void sortLines(Comparator<List<String>> comparator) {
		lines = new ArrayList<>(lines.stream().sorted(comparator).toList());
	}

	public CSVDocument convertType(CSVDocumentType newType, Function<List<String>, List<String>> converter) {
		CSVDocument newDocument = new CSVDocument(newType);
		for(List<String> line: lines) {
			newDocument.addLine(converter.apply(line));
		}
		return newDocument;
	}

	public void mergeFile(CSVDocument otherDocument) {
		lines.addAll(otherDocument.lines);
		lines = new ArrayList<>(lines.stream().distinct().sorted(documentType.getComparator()).toList());
	}

	public Comparator<List<String>> getComparator() {
		return documentType.getComparator();
	}

}
