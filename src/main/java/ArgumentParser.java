import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ArgumentParser {
	private CommandLine cmd;
	private static final Logger LOG = LoggerFactory.getLogger(ArgumentParser.class);

	public void parseArguments(String[] args) {
		Options options = new Options();
		Option outputFileOption = new Option("o", "outputFile", true, "output file path");
		outputFileOption.setRequired(true);
		options.addOption(outputFileOption);
		Option inputFilesOption = new Option("i", "inputFiles", true, "input files");
		inputFilesOption.setArgs(Option.UNLIMITED_VALUES);
		inputFilesOption.setRequired(true);
		Option currentFile = new Option("c", "currentFile", true, "current file");
		options.addOption(currentFile);
		options.addOption(inputFilesOption);
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			LOG.info(e.getMessage());
			formatter.printHelp("paramteres", options);
			System.exit(-1);
		}
	}

	public Path getOutputFile() {
		return Path.of(cmd.getOptionValue("outputFile"));
	}

	public List<Path> getInputFiles() {
		return Arrays.stream(cmd.getOptionValues("inputFiles")).map(Path::of).toList();
	}

	public Optional<Path> getCurrentFile() {
		String curFile = cmd.getOptionValue("currentFile");
		if(curFile == null){
			return Optional.empty();
		} else {
			return Optional.of(Path.of(curFile));
		}
	}
}
