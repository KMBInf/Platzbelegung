import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Platzbelegung {

	public static void main(String[] args) throws CSVParserException, IOException {
		ArgumentParser parser = new ArgumentParser();
		parser.parseArguments(args);

		Path outputFile = parser.getOutputFile();
		List<Path> inputFiles = parser.getInputFiles();
		validateInputPaths(inputFiles);
		CSVDocument inputDocument = new CSVDocument(new SpielplusCSVDocument());
		inputDocument.readLinesFromFile(inputFiles.get(0), StandardCharsets.UTF_16);
		for(int i = 1; i < inputFiles.size(); i++){
			CSVDocument doc = new CSVDocument(new SpielplusCSVDocument());
			doc.readLinesFromFile(inputFiles.get(i), StandardCharsets.UTF_16);
			inputDocument.mergeFile(doc);
		}
		CSVDocument outputDocument = CSVConverter.convertSpielplusToGoogleDocCsv(inputDocument);
		if(parser.getCurrentFile().isPresent()){
			CSVDocument currentFile = new CSVDocument(new GoogleDocDocumentType());
			currentFile.readLinesFromFile(parser.getCurrentFile().get(), StandardCharsets.UTF_8);
			outputDocument.mergeFile(currentFile);
		}

		outputDocument.writeLinesToFile(outputFile);
	}

	private static void validateInputPaths(List<Path> paths){
		for(Path path: paths) {
			if(!Files.exists(path)){
				throw new IllegalArgumentException(path + " does not exist.");
			}
		}
	}
}
