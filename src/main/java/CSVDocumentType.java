import java.util.Comparator;
import java.util.List;

public interface CSVDocumentType {
	List<String> getHeadline();
	String getSeparator();
	Comparator<List<String>> getComparator();
}
