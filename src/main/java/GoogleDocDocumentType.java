import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class GoogleDocDocumentType implements CSVDocumentType {

	@Override
	public List<String> getHeadline() {
		return List.of("Wochentag", "Spieldatum", "Uhrzeit", "Heimmannschaft", "Gastmannschaft", "Mannschaftsart", "Liga", "Platz");
	}

	@Override
	public String getSeparator() {
		return ",";
	}

	@Override
	public Comparator<List<String>> getComparator() {
		return GoogleDocDocumentType::sortDate;
	}
	private static int sortDate(List<String> list1, List<String> list2) {
		LocalDate date1 = getDateFromLine(list1);
		LocalDate date2 = getDateFromLine(list2);
		return date1.compareTo(date2);

	}

	private static LocalDate getDateFromLine(List<String> line) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm", Locale.GERMANY);
		String dateString = GoogleDocDocumentType.getDatum(line) + " " + GoogleDocDocumentType.getUhrzeit(line);
		return LocalDate.parse(dateString, formatter);
	}

	private static String getUhrzeit(List<String> line) {
		return line.get(2);
	}

	private static String getDatum(List<String> line) {
		return line.get(1);
	}

	public static String getHeimmannschaft(List<String> line) {
		return line.get(3);
	}

	public static String getGastmannschaft(List<String> line) {
		return line.get(4);
	}
}
