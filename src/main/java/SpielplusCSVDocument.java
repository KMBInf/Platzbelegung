import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class SpielplusCSVDocument implements CSVDocumentType {
	@Override
	public List<String> getHeadline() {
		//Saison	Verband	Spielgebiet	Mannschaftsart	Staffel	Spielstätte	Spielstätten-Nr.	Straße/Hausnr.	PLZ	Ort	Platznummer	Typ	Größe	Max. parallele Spiele	Max. Spiele/Tag	Max. Spiele/Wochenende	Früheste Anstoßzeit	Späteste Anstoßzeit	Mittagspause	Wochentag	Spieldatum	Uhrzeit	Sptg.	Spielkennung	Typ	Liga	Heimmannschaft	Gastmannschaft	Spielleitung	Assistent 1	Assistent 2	SpielleitungAusweisnr.	Assistent 1Ausweisnr.	Assistent 2Ausweisnr.	SpielleitungSchirigebiet	Assistent 1Schirigebiet	Assistent 2Schirigebiet
		return List.of("Saison", "Verband", "Spielgebiet", "Mannschaftsart", "Staffel", "Spielstätte", "Spielstätten-Nr.", "Straße/Hausnr.", "PLZ", "Ort", "Platznummer", "Typ", "Größe", "Max. parallele Spiele", "Max. Spiele/Tag", "Max. Spiele/Wochenende", "Früheste Anstoßzeit", "Späteste Anstoßzeit", "Mittagspause", "Wochentag", "Spieldatum", "Uhrzeit", "Sptg.", "Spielkennung", "Typ", "Liga", "Heimmannschaft", "Gastmannschaft", "Spielleitung", "Assistent 1", "Assistent 2", "SpielleitungAusweisnr.", "Assistent 1Ausweisnr.", "Assistent 2Ausweisnr.", "SpielleitungSchirigebiet", "Assistent 1Schirigebiet", "Assistent 2Schirigebiet");
	}

	@Override
	public String getSeparator() {
		return "\t";
	}

	@Override
	public Comparator<List<String>> getComparator() {
		return SpielplusCSVDocument::sortDate;
	}

	private static int sortDate(List<String> list1, List<String> list2) {
		LocalDate date1 = getDateFromLine(list1);
		LocalDate date2 = getDateFromLine(list2);
		return date1.compareTo(date2);

	}

	private static LocalDate getDateFromLine(List<String> line) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm", Locale.GERMANY);
		String dateString = SpielplusCSVDocument.getDatum(line) + " " + SpielplusCSVDocument.getUhrzeit(line);
		return LocalDate.parse(dateString, formatter);
	}

	public static String getHeimmannschaft(List<String> line) {
		return line.get(26);
	}

	public static String getGastmannschaft(List<String> line) {
		return line.get(27);
	}

	public static String getSpielStaette(List<String> line) {
		return line.get(5);
	}

	public static String getSpielfeld(List<String> line) {
		return line.get(5).split(",")[1];
	}

	public static String getMannschaft(List<String> line) {
		return line.get(3);
	}

	public static String getSpieltyp(List<String> line) {
		return line.get(4);
	}

	public static String getWochentag(List<String> line) {
		return line.get(19);
	}

	public static String getUhrzeit(List<String> line) {
		return line.get(21);
	}

	public static String getDatum(List<String> line) {
		return line.get(20);
	}
}
