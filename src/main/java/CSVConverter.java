import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CSVConverter {
	public static CSVDocument convertSpielplusToGoogleDocCsv(CSVDocument inputDocument) {
		inputDocument.filterLines((list) -> SpielplusCSVDocument.getSpielStaette(list).split(",")[0].equals("Sportanlage Gaustadt"));
		inputDocument.sortLines(inputDocument.getComparator());
		CSVDocument googleDoc = inputDocument.convertType(new GoogleDocDocumentType(), CSVConverter::convertSpielplusToGoogleLine);
		googleDoc.filterLines(CSVConverter::filterNonGaustadtTurnier);
		return googleDoc;
	}

	private static boolean filterNonGaustadtTurnier(List<String> line) {
		if(!GoogleDocDocumentType.getHeimmannschaft(line).contains("Kinderfestival")){
			return true;
		}
		return GoogleDocDocumentType.getGastmannschaft(line).contains("Gaustadt");
	}

	private static List<String> convertSpielplusToGoogleLine(List<String> line) {
		return List.of(SpielplusCSVDocument.getWochentag(line), SpielplusCSVDocument.getDatum(line),
				SpielplusCSVDocument.getUhrzeit(line), SpielplusCSVDocument.getHeimmannschaft(line),
				SpielplusCSVDocument.getGastmannschaft(line), SpielplusCSVDocument.getMannschaft(line),
				SpielplusCSVDocument.getSpieltyp(line), spielort(SpielplusCSVDocument.getSpielfeld(line)));
	}

	private static String spielort(String feld) {
		return switch(feld) {
			case " Platz 1" -> "Naturrasen";
			case " Kunstrasen" -> "Kunstrasen";
			default -> throw new IllegalStateException("Keine Registrierte Spielstaette");
		};
	}

}
